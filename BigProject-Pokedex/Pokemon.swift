//
//  Pokemon.swift
//  BigProject-Pokedex
//
//  Created by Mac on 06/04/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation
import Alamofire

class Pokemon {
    private var _name: String! //
    private var _pokedexId: Int! //
    private var _desc: String!
    private var _type: String!
    private var _defense: String!
    private var _height: String!
    private var _weight: String!
    private var _attack: String!
    private var _nextEvolutionName: String!
    private var _nextEvolutionId: String!
    private var _nextEvolutionLvl: String!
    private var _pokemonUrl: String!
    
    var name: String {
        get {
            if _name == nil {
                _name = ""
            }
            return _name
        }
    }
    
    var pokedexId: Int {
        get {
            if _pokedexId == nil {
                _pokedexId = 0
            }
            return _pokedexId
        }
    }
    
    var description: String {
        get {
            if _desc == nil {
                _desc = ""
            }
            return _desc
        }
    }
    
    var type: String {
        get {
            if _type == nil {
                _type = ""
            }
            return _type
        }
    }
    
    var defense: String {
        get {
            if _defense == nil {
                _defense = ""
            }
            return _defense
        }
    }
    
    var height: String {
        get {
            if _height == nil {
                _height = ""
            }
            return _height
        }
    }
    
    var weight: String {
        get {
            if _weight == nil {
                _weight = ""
            }
            return _weight
        }
    }
    
    var attack: String {
        get {
            if _attack == nil {
                _attack = ""
            }
            return _attack
        }
    }
    
    var nextEvoName: String {
        get {
            if _nextEvolutionName == nil {
                _nextEvolutionName = ""
            }
            return _nextEvolutionName
        }
    }
    
    var nextEvoId: String {
        get {
            if _nextEvolutionId == nil {
                _nextEvolutionId = ""
            }
            return _nextEvolutionId
        }
    }
    
    var nextEvoLvl: String {
        get {
            if _nextEvolutionLvl == nil {
                _nextEvolutionLvl = ""
            }
            return _nextEvolutionLvl
        }
    }

    init(name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
        
        _pokemonUrl = "\(URL_BASE)\(URL_POKEMON)\(self._pokedexId)/"
    }
    
    func downloadPokemonDetails(completed: DownloadComplete) {
        
        let url = NSURL(string: _pokemonUrl)!
        Alamofire.request(.GET, url).responseJSON { (response: Response<AnyObject, NSError>) in
            let result = response.result
            
            if let dict = result.value as? Dictionary<String, AnyObject> {
            
                if let weight = dict["weight"] as? String {
                    self._weight = weight
                }
                
                if let height = dict["height"] as? String {
                    self._height = height
                }
                
                if let attack = dict["attack"] as? Int {
                    self._attack = "\(attack)"
                }
                
                if let defense = dict["defense"] as? Int {
                    self._defense = "\(defense)"
                }
                
                print(self._weight)
                print(self._defense)
                print(self._attack)
                print(self._height)
                
                
                if let types = dict["types"] as? [Dictionary<String, String>] where types.count > 0 {
                    if let name = types[0]["name"] {
                        self._type = name.capitalizedString
                    }
                    
                    if types.count > 1 {
                        for x in 1 ..< types.count {
                            if let name = types[x]["name"] {
                                self._type! += " / \(name.capitalizedString)"
                            }
                        }
                    }
                    
                } else {
                    self._type = "None"
                }
                
            print(self._type)
                
                if let descArr = dict["descriptions"] as? [Dictionary<String, AnyObject>] where descArr.count > 0 {
                    if let descUrl = descArr[0]["resource_uri"] as? String {
                        let nsUrl = NSURL(string: "\(URL_BASE)\(descUrl)")!
                        
                        Alamofire.request(.GET, nsUrl).responseJSON { (response: Response<AnyObject, NSError>) in
                            let result = response.result
                            if let descDict = result.value as? Dictionary<String, AnyObject> {
                                if let desc = descDict["description"] as? String {
                                    self._desc = desc
                                    print(self._desc)
                                }
                            }
                            
                            completed()
                            
                        }
                    }
                }
                
                if let evolutions = dict["evolutions"] as? [Dictionary<String, AnyObject>] where evolutions.count > 0 {
                 
                    if let to = evolutions[0]["to"] as? String, let levelEvo = evolutions[0]["level"] as? Int {
                     
                        // Can't support mega pokemon right now but
                        // API still has mega data
                        if to.rangeOfString("mega") == nil {
                            
                            if let uri = evolutions[0]["resource_uri"] as? String {
                                let newStr = uri.stringByReplacingOccurrencesOfString("/api/v1/pokemon/", withString: "")
                                let nextEvolutionId = newStr.stringByReplacingOccurrencesOfString("/", withString: "")
                                
                                self._nextEvolutionId = nextEvolutionId
                                self._nextEvolutionName = to
                                self._nextEvolutionLvl = "\(levelEvo)"
                                
                                print(self._nextEvolutionId)
                                print(self._nextEvolutionName)
                            }
                        }
                    }
                }
            }
        }
    }
}