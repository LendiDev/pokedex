//
//  Constains.swift
//  BigProject-Pokedex
//
//  Created by Mac on 11/04/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"
let URL_DESC = "/api/v1/description/"

typealias DownloadComplete = () -> ()