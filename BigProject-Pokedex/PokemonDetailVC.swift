//
//  PokemonDetailVC.swift
//  BigProject-Pokedex
//
//  Created by Mac on 11/04/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {

    @IBOutlet weak var pokemonLbl: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel! //
    @IBOutlet weak var defenceLbl: UILabel! //
    @IBOutlet weak var heightLbl: UILabel! //
    @IBOutlet weak var weightLbl: UILabel! //
    @IBOutlet weak var pokedexLbl: UILabel! 
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var currentEvoImg: UIImageView!
    @IBOutlet weak var nextEvoImg: UIImageView!
    @IBOutlet weak var evoLbl: UILabel!
    @IBOutlet weak var loadBg: UIView!
    @IBOutlet weak var loadActivity: UIActivityIndicatorView!
    
    var pokemon: Pokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pokemonLbl.text = pokemon.name.capitalizedString
        mainImg.image = UIImage(named: "\(pokemon.pokedexId)")
        currentEvoImg.image = mainImg.image
        
        loadActivity.startAnimating()
        
        pokemon.downloadPokemonDetails {
            // this will be called after download is done
            self.updateUI()
        }
    }

    func updateUI() {
        heightLbl.text = pokemon.height
        attackLbl.text = pokemon.attack
        typeLbl.text = pokemon.type
        weightLbl.text = pokemon.weight
        defenceLbl.text = pokemon.defense
        descriptionLbl.text = pokemon.description
        pokedexLbl.text = "\(pokemon.pokedexId)"
        
        if pokemon.nextEvoId == "" {
            evoLbl.text = "No Evolutions"
            nextEvoImg.hidden = true
        } else {
            nextEvoImg.hidden = false
            nextEvoImg.image = UIImage(named: pokemon.nextEvoId)
            
            var str = "Next Evolution: \(pokemon.nextEvoName)"
            if pokemon.nextEvoLvl != "" {
                str += " - LVL \(pokemon.nextEvoLvl)"
            }
            evoLbl.text = str
        }
        
        loadBg.hidden = true
        loadActivity.stopAnimating()
        loadActivity.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
