//
//  SearchBar.swift
//  BigProject-Pokedex
//
//  Created by Mac on 06/04/2016.
//  Copyright © 2016 Mac. All rights reserved.
//

import UIKit

class SearchBar: UISearchBar {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Change Placeholder text color
        //self.setValue(UIColor.grayColor(), forKeyPath: "_searchField._placeholderLabel.textColor")
        
        // Change typing text color
        self.setValue(UIColor.whiteColor(), forKeyPath: "_searchField.textColor")
    }
    
    override func setShowsCancelButton(showsCancelButton: Bool, animated: Bool) {
        // do nothing
    }

}
